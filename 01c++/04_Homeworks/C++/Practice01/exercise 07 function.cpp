//Autor: Lu Bona
// formula: F = G*masa1*masa2 / distancia2 y G = 6.673 * 10-8 cm3/g.seg2

#include<iostream>
#include<conio.h>
using namespace std;
float strength(float distance, float mass1, float mass2) ;
int main(){
	double G = 6.673 ;//constant
	float distance = 0.0; // distance
	float mass1 = 0.0; // First mass
	float mass2 = 0.0; // Second mass
	float n; //save function
	//Display of algorithm
	cout<<"Write the distance down: ";
	cin>>distance;
	
	cout<<"\nWrite the first mass down: ";
	cin>>mass1;
	
	cout<<"\nWrite the second mass down: ";
	cin>>mass2;
	
	//Display the function
	
	n=strength(distance,mass1,mass2);
	
	
	//Final part of the algorithm
	cout<<"\nThe attraction force is: ";
	cout<<n<<" Newtons";
	
	getch();
	return 0;
}

float strength(float distance,float mass1, float mass2){
	float G = 6.673; //Constant
	float str = 0.0; //stregth
	str=(G*mass1*mass2)/distance;
	
	return str;
}
