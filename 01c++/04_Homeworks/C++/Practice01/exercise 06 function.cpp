//Autor: Lu Bona
// Desglosar cierta cantidad de segundos a su equivalente en d�as, horas, minutos y segundos

#include<iostream>
#include<conio.h>

using namespace std;

void time(int,int&,int&,int&);

int main(){
	int totalSec = 0; // Here we save our data for changing our time
	int hours = 0; //Here we save our hours
	int min = 0; //Here we save our minutes
	int sec = 0; // Here we save our seconds
	
	//Display of the first part
	cout<<"Write the quantity of seconds down: ";
	cin>>totalSec;
	
		//Display of the function
	time(totalSec,hours,min,sec);
	
//Display of algorithm
	cout<<"\nHoras: "<<hours<<endl;
	cout<<"Minutos: "<<min<<endl;
	cout<<"Segundos: "<<sec<<endl;
	
	getch();
	return 0;
}

// This function will do all the process
void time(int totalSec,int& hours,int& min,int& sec){	
	hours = totalSec/3600;
	totalSec %= 3600;
	min = totalSec/60;
	sec = totalSec%60; 
}

